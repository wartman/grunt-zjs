'use strict';

var grunt = require('grunt');

exports.zjs = {
  setUp: function(done) {
    // setup here if necessary
    done();
  },
  export_item: function (test) {
    var actual = grunt.file.read(__dirname + "/tmp/app.js");
    var expected = grunt.file.read(__dirname + "/fixtures/expected/app.js");
    test.equal(actual, expected, 'Module compiled as expected');

    test.done();
  },
};
