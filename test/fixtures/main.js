z.config('root', 'test/fixtures/');
z.config('main', 'app.main');

z('app.main').
imports('foo.bar').
imports('foo.bin').
exports(function(){
  this.Foo = 'foo';
  this.Bin = foo.bin;
  this.Bar = foo.bar.Bar;
});