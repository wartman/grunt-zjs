(function () {
var root = this;
/* namespaces */
var app = this.app = {};
var foo = this.foo = {};

/* modules */
foo.bax = (function () {
var exports = (function (){
    return "bax";
  })();
return exports;
})();
foo.bin = (function () {
var exports = (function (){
    this.Bax = foo.bax;
    this.Bin = "Bin";
  })();
return exports;
})();
foo.bar = (function () {
var exports = (function () {
    this.Bar = "FooBar";
  })();
return exports;
})();
app.main = (function () {
var exports = (function (){
  this.Foo = 'foo';
  this.Bin = foo.bin;
  this.Bar = foo.bar.Bar;
})();
return exports;
})();

}).call(this);