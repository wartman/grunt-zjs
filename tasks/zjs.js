/*
 * grunt-zjs
 * 
 *
 * Copyright (c) 2014 Peter Wartman
 * Licensed under the MIT license.
 */

'use strict';

var Build = require('zjs/src/build');

module.exports = function (grunt) {

  grunt.registerMultiTask('zjs', 'zjs compiler for grunt', function() {

    var options = this.data.options;
    var src = this.data.src;
    var dest = this.data.dest;

    var done = this.async();

    var build = new Build(options);
    build.start(src, dest);
    build.done( function () {
      done();
      grunt.log.ok();
    });

  });

};
